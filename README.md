# Milie's Map Maker
This is the map maker used by Federation and I've decided to make it open-source and public.
The code is extremely messy, by the way.

## How to Use
Keep in mind that because this is still a fairly new program and I haven't worked much on it, there are a lot
of small bugs. 

In order to add a new guild, press the [ + ADD GUILD ] button in the right menu. Type in the guild's
prefix and assign it a color in the pop-up menu. 

To assign a guild territories, drag your mouse over a cluster of territories and you will
notice those territories have been "highlighted". You can assign "highlighted" territories
to a guild by then clicking on the guild's name in the right menu.

After you make a map, you must click the [ SAVE ] button to save it. If you do not, the 
map will not be there anymore.

To delete a guild from the menu, remove all their territories then save your map. Then, 
you must open the "saveColors.txt" file, which is in the same folder as the map maker 
application. Delete the part that has the guild name and its color. 

## Notes
Feel free to give me any suggestions or ideas for this project! I'm planning to keep updating 
and improving this application in the future.

## License
This repository has been licensed with the MIT License.
